var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');

var log4js = require('log4js');
log4js.configure({ "appenders": [ {
    "type": "dateFile",
    "filename": "logs/logger.log",
    "pattern": "-yyyy-MM-dd-hh.log",
    "category": "mean_server"
  },
  { type: 'console' }]
});

var logger = log4js.getLogger('mean_server');

/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
//app.use(expressValidator());

/*MySql connection*/
var connection  = require('express-myconnection'),
    mysql = require('mysql');

app.use(

    connection(mysql,{
    	 host: 'localhost',
         user: 'root',
         password : 'root',
         port : 3306, //port mysql
         database:'nodejs',
        debug    : false //set true if you wanna see debug logger
    },'request')

);

app.get('/',function(req,res){
   // res.send('Welcome');
	res.render('user',{title:"RESTful Crud Example"});
});


//RESTful route
var router = express.Router();


/*------------------------------------------------------
*  This is router middleware,invoked everytime
*  we hit url /api and anything after /api
*  like /api/user , /api/user/7
*  we can use this for doing validation,authetication
*  for every route started with /api
--------------------------------------------------------*/
router.use(function(req, res, next) {
    logger.info(req.method, req.url);
    next();
});

var curut = router.route('/user');


//show the CRUD interface | GET
curut.get(function(req,res,next){


    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query('SELECT * FROM user',function(err,rows){

            if(err){
                logger.info(err);
                return next("Mysql error, check your query");
            }

            logger.info(rows);
            res.send(rows);

         });

    });

});
//post data to DB | POST
curut.post(function(req,res,next){

    //validation
   /* req.assert('name','Name is required').notEmpty();
    req.assert('email','A valid email is required').isEmail();
    req.assert('password','Enter a password 6 - 20').len(6,20);

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }*/

    //get data
    var data = {
        name:req.body.name,
        email:req.body.email,
        address:req.body.address
     };

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("INSERT INTO user set ? ",data, function(err, rows){

           if(err){
                logger.info(err);
                return next("Mysql error, check your query");
           }

          res.sendStatus(200);

        });

     });

});


//now for Single route (GET,DELETE,PUT)
var curut2 = router.route('/user/:user_id');

/*------------------------------------------------------
route.all is extremely useful. you can use it to do
stuffs for specific routes. for example you need to do
a validation everytime route /api/user/:user_id it hit.

remove curut2.all() if you dont want it
------------------------------------------------------*/
curut2.all(function(req,res,next){
    logger.info("You need to smth about curut2 Route ? Do it here");
    logger.info(req.params);
    next();
});

//get data to update
curut2.get(function(req,res,next){

    var user_id = req.params.user_id;

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM user WHERE user_id = ? ",[user_id],function(err,rows){

            if(err){
                logger.info(err);
                return next("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("User Not found");

            res.send(rows);
        });

    });

});

//update data
curut2.put(function(req,res,next){
    var user_id = req.params.user_id;

    //validation
 /*   req.assert('name','Name is required').notEmpty();
    req.assert('email','A valid email is required').isEmail();
    req.assert('password','Enter a password 6 - 20').len(6,20);

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }*/

    //get data
    var data = {
        name:req.body.name,
        email:req.body.email,
        address:req.body.address
     };

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("UPDATE user set ? WHERE user_id = ? ",[data,user_id], function(err, rows){

           if(err){
                logger.info(err);
                return next("Mysql error, check your query");
           }

          res.sendStatus(200);

        });

     });

});

//delete data
curut2.delete(function(req,res,next){

    var user_id = req.params.user_id;

     req.getConnection(function (err, conn) {

        if (err) return next("Cannot Connect");

        var query = conn.query("DELETE FROM user  WHERE user_id = ? ",[user_id], function(err, rows){

             if(err){
                logger.info(err);
                return next("Mysql error, check your query");
             }

             res.sendStatus(200);

        });
        //logger.info(query.sql);

     });
});

//now we need to apply our router here
app.use('/api', router);

//start Server
var server = app.listen(8080,function(){

   logger.info("Listening to port %s",server.address().port);

});
